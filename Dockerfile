FROM alpine:latest as build

ENV GOPATH /go
ENV GOROOT /usr/lib/go
ENV PATH /go/bin:$PATH

RUN apk add --no-cache git go sqlite
COPY . /hypixel-bot
WORKDIR /hypixel-bot
RUN test -f db.sql || sqlite3 db.sql "CREATE TABLE users(id INT PRIMARY KEY NOT NULL, name TEXT, cock int, last_cock int, register_date int);"
RUN go build -o hypixel-bot ./cmd

FROM alpine:latest 
COPY --from=build hypixel-bot/hypixel-bot .
COPY --from=build hypixel-bot/db.sql .
EXPOSE 8080
CMD ["./hypixel-bot"]
