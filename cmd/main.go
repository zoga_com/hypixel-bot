package main

import (
	"hypixel-bot/pkg/bot"
	"hypixel-bot/pkg/poll"
	"hypixel-bot/pkg/util"
	"os"
)

func main() {
	bot.New(os.Getenv("VK_TOKEN"), os.Getenv("HYPIXEL_KEY"))
	util.Cache = map[string]util.Mojang{}

	bot.VK.EnableZstd()
	bot.VK.EnableMessagePack()

	poll.Poll()
}
