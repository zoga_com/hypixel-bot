package commands

import (
	"bytes"
	"hypixel-bot/pkg/util"
	"regexp"
	"text/template"
)

var swtmpl = template.Must(template.ParseFiles("tmpl/sw.gohtml"))

var Skywars = &Command{
	Name:      regexp.MustCompile("^(skywars|св|скайварс|sw)$"),
	Args:      1,
	UseDBName: true,
	ForAdmins: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		player, err := util.GetPlayer(args[0])
		if err != nil {
			return
		}

		buf := &bytes.Buffer{}
		swtmpl.Execute(buf, player)

		err = util.SendMessage(peer_id, buf.String())
		return
	},
}
