package commands

import (
	"bytes"
	"hypixel-bot/pkg/util"
	"regexp"
	"text/template"
)

var bwtmpl = template.Must(template.ParseFiles("tmpl/bw.gohtml"))

var Bedwars = &Command{
	Name:      regexp.MustCompile("^(бв|бедварс|bw|bedwars)$"),
	Args:      1,
	ForAdmins: false,
	UseDBName: true,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		player, err := util.GetPlayer(args[0])
		if err != nil {
			return
		}

		buf := &bytes.Buffer{}
		err = bwtmpl.Execute(buf, player)
		if err != nil {
			return
		}

		err = util.SendMessage(peer_id, buf.String())
		if err != nil {
			return
		}
		return
	},
}
