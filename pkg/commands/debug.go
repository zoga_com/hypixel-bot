package commands

import (
	"fmt"
	"regexp"
	"runtime"

	"hypixel-bot/pkg/util"
)

var Debug = &Command{
	Name:      regexp.MustCompile("^(debug|дебаг|info)$"),
	Args:      0,
	ForAdmins: true,
	UseDBName: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		m := &runtime.MemStats{}
		runtime.ReadMemStats(m)

		message := fmt.Sprintf(`
		Debug info:

		Go version: %s
		CPUs: %d (%s)
		Mem: %.3f MB
		Next GC: %.3f MB



		`, runtime.Version(), runtime.NumCPU(), runtime.GOOS,
			toMB(m.Alloc), toMB(m.NextGC),
		)

		util.SendMessage(peer_id, message)
		return
	},
}

func toMB(i uint64) float64 {
	return float64(i) / 1049000
}
