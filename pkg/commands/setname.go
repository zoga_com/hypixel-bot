package commands

import (
	"hypixel-bot/pkg/bot"
	"hypixel-bot/pkg/util"
	"regexp"

	_ "modernc.org/sqlite"
)

var Nick = &Command{
	Name:      regexp.MustCompile("^(ник|имя|nick|name|setname|setnick)$"),
	Args:      1,
	ForAdmins: false,
	UseDBName: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		mojang, err := util.GetUUID(args[0])
		if err != nil {
			return
		}

		statement, err := bot.DB.Prepare(`INSERT INTO users (id, name) VALUES (?, ?) ON CONFLICT(id) DO UPDATE SET id = ?, name = ?`)
		if err != nil {
			return
		}

		_, err = statement.Exec(from_id, mojang.Name, from_id, mojang.Name)
		if err != nil {
			return
		}

		return
	},
}
