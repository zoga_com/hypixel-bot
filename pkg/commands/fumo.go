package commands

import (
	"regexp"

	"hypixel-bot/pkg/util"
)

var Fumo = &Command{
	Name:      regexp.MustCompile("^(fumo|фумо)$"),
	Args:      0,
	ForAdmins: false,
	UseDBName: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		return util.SendMessage(peer_id, "фумо завтра приедет")
	},
}
