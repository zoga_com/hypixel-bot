package commands

import (
	"fmt"
	"hypixel-bot/pkg/util"
	"regexp"
)

var RankedSkywars = &Command{
	Name:      regexp.MustCompile("^(ranked|рсв|ранкед|rsw)$"),
	Args:      1,
	UseDBName: true,
	ForAdmins: false,
	Trigger: func(args []string, peer_id int, from_id int) (err error) {
		uuid, err := util.GetUUID(args[0])
		if err != nil {
			return
		}

		ranked, err := util.GetRanked(uuid.Id)
		if err != nil {
			return
		}

		message := fmt.Sprintf("Ranked статистика %s:\nКлюч: %s\nПозиция: %d\nОчки: %d", uuid.Name, ranked.Ranked.Key, ranked.Ranked.Position, ranked.Ranked.Score)

		err = util.SendMessage(peer_id, message)
		return
	},
}
